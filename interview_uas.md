# Business Pitch

## Jawaban UAS

Sourcode:

- [frontend](https://gitlab.com/lahitanianissa32/pbotiktok.git)
- [backend](https://gitlab.com/lahitanianissa32/webservice_pbo.git)


# No 1

Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:

*Jawaban*

- **Use Case Pengguna**

| No. | Use Case                                                    | Nilai Prioritas | Aktor                    | Tersedia |
|-----|------------------------------------------------------------|-----------------|--------------------------|----------|
| 1   | Pembuatan dan Pengunggahan konten                           | 10              | Pengguna                 | Yes      |
| 2   | Penayangan konten                                           | 10              | Pengguna                 | Yes      |
| 3   | Fitur reaksi (Like video)                                   | 9               | Pengguna                 | Yes      |
| 4   | Fitur reaksi (Comment video)                                | 9               | Pengguna                 | Yes      |
| 5   | Fitur reaksi (Save video)                                   | 9               | Pengguna                 | Yes      |
| 6   | Fitur reaksi (Share video)                                  | 9               | Pengguna                 | Yes      |
| 7   | Fitur Live                                                  | 8               | Pengguna                 | No       |
| 8   | Fitur Pembelian barang di Tiktok shop                       | 8               | Pengguna                 | No       |
| 9   | Penggunaan filter video                                     | 8               | Pengguna                 | No       |
| 10  | Penggunaan efek video                                       | 8               | Pengguna                 | No       |
| 11  | Mengelola preferensi konten                                 | 8               | Pengguna                 | No       |
| 12  | Fitur Pencarian dan Penemuan Konten                         | 8               | Pengguna                 | Yes      |
| 13  | Menambahkan teks deskripsi                                  | 7               | Pengguna                 | Yes      |
| 14  | Fitur Duet                                                  | 7               | Pengguna                 | No       |
| 15  | Membuat konten dengan musik tertentu                        | 7               | Pengguna                 | Yes      |
| 16  | Mengikuti akun pengguna lain                                | 7               | Pengguna                 | Yes      |
| 17  | Mengelola akun dan profil pengguna                          | 7               | Pengguna                 | Yes      |
| 18  | Mengelola pesan dan percakapan                              | 7               | Pengguna                 | No       |
| 19  | Fitur melihat riwayat pembelian di Tiktok shop              | 7               | Pengguna                 | No       |
| 20  | Fitur tambah saldo di Tiktok shop                           | 6               | Pengguna                 | No       |
| 21  | Influencer marketing                                         | 6               | Pengguna                | No       |
| 22  | Menambahkan stiker dan emoji video                           | 6               | Pengguna                | No       |
| 23  | Menonton video dengan hastag                                | 6               | Pengguna                 | No       |
| 24  | Tambahkan keranjang kuning                                   | 6               | Pengguna                | No       |
| 25  | Fitur Kolaborasi                                            | 6               | Pengguna                 | No       |
| 26  | Login                                                       | 6               | Pengguna                 | Yes      |
| 27  | Logout                                                      | 6               | Pengguna                 | Yes      |
| 28  | Penambahan tagar pada video                                 | 5               | Pengguna                 | Yes      |
| 29  | Menandai teman di komentar atau deskripsi                    | 5               | Pengguna                | No       |
| 30  | Like komentar                                               | 5               | Pengguna                 | Yes      |
| 31  | Memblokir pengguna                                          | 5               | Pengguna                 | No       |
| 32  | Mengelola notifikasi                                        | 5               | Pengguna                 | No       |
| 33  | Fitur berbagi video dalam pesan                             | 5               | Pengguna                 | No       |
| 34  | Mengatur pengaturan suara                                   | 5               | Pengguna                 | No       |


- **Use Case Manajemen**

| No. | Use Case                                                    | Nilai Prioritas | Aktor                    | Tersedia |
|-----|------------------------------------------------------------|-----------------|--------------------------|----------|
| 35  | Pengelolaan konten yang melanggar kebijakan komunitas        | 9               | Manajemen                | No       |
| 36  | Analisis data pengguna dan aktivitas                        | 9               | Manajemen                | No       |
| 37  | Pemantauan konten dan penegakan hukum                        | 9               | Manajemen                | No       |
| 38  | Peningkatan kecepatan dan performa platform                  | 9               | Manajemen                | No       |
| 39  | Optimasi fitur privasi dan keamanan                         | 9               | Manajemen                | No       |
| 40  | Pengembangan dan pengujian fitur baru                       | 9               | Manajemen                | No       |
| 41  | Survei dan riset pasar                                      | 8               | Manajemen                | No       |
| 42  | Pengelolaan sistem filter                                    | 8               | Manajemen                | No       |
| 43  | Analisis data pengguna dan aktivitas untuk iklan terarget    | 8               | Manajemen                | No       |
| 44  | Peningkatan algoritma rekomendasi konten                     | 8               | Manajemen                | No       |
| 45  | Pengelolaan masukan dan umpan balik pengguna                | 8               | Manajemen                | No       |
| 46  | Pengelolaan kata sandi                                      | 5               | Manajemen                | No       |
| 47  | Kemitraan dengan agensi periklanan                           | 7               | Manajemen                | No       |
| 48  | Manajemen hubungan dan kemitraan dengan merek, selebriti, dan influencer | 7 | Manajemen | No |
| 49  | Pengelolaan program penghargaan dan promosi                  | 7               | Manajemen                | No       |
| 50  | Kampanye pemasaran dan promosi                              | 7               | Manajemen                | No       |
| 51  | Pengelolaan sistem pembayaran dan komisi                     | 6               | Manajemen                | No       |
| 52  | Penyelenggaraan acara dan kompetisi                          | 6               | Manajemen                | No       |
| 53  | Dukungan pelanggan                                          | 6               | Manajemen                | No       |
| 54  | Mengelola komunitas dan aturan perilaku                      | 5               | Manajemen                | No       |
| 55  | Peningkatan keamanan platform                               | 5               | Manajemen                | No       |
| 56  | Penanganan laporan pelanggaran                              | 5               | Manajemen                | No       |
| 57  | Pengelolaan pemulihan                                       | 7               | Manajemen                | No       |
| 58  | Pembaruan kebijakan dan persyaratan                          | 5               | Manajemen                | No       |


- **Use Case Direksi Perusahaan**

| No. | Use Case                                                    | Nilai Prioritas | Aktor                    | Tersedia |
|-----|------------------------------------------------------------|-----------------|-------------------------- |----------|
| 59  | Analisis kinerja platform TikTok                            | 9               | Direksi Perusahaan       | No       |
| 60  | Pengembangan strategi pertumbuhan dan ekspansi bisnis       | 9               | Direksi Perusahaan       | No       |
| 61  | Pengawasan dan pengendalian keuangan perusahaan              | 9               | Direksi Perusahaan      | No       |
| 62  | Pengambilan keputusan strategis untuk pengembangan produk   | 9               | Direksi Perusahaan       | No       |
| 63  | Pelaporan dan komunikasi kepada pemangku kepentingan         | 9               | Direksi Perusahaan      | No       |
| 64  | Riset pasar dan analisis tren konsumen                      | 8               | Direksi Perusahaan       | No       |
| 65  | Pengembangan kemitraan strategis                             | 8               | Direksi Perusahaan       | No       |
| 66  | Membuat dan melaksanakan kebijakan perusahaan                | 8               | Direksi Perusahaan       | No       |
| 67  | Pengelolaan risiko dan kepatuhan regulasi                    | 8               | Direksi Perusahaan       | No       |
| 68  | Mengawasi dan memantau pelaksanaan rencana bisnis            | 8               | Direksi Perusahaan       | No       |
| 69  | Pengembangan dan pelaksanaan strategi pemasaran              | 7               | Direksi Perusahaan       | No       |
| 70  | Evaluasi dan pemantauan performa departemen dan tim kerja    | 7               | Direksi Perusahaan       | No       |
| 71  | Mengelola hubungan dengan pemegang saham dan investor        | 7               | Direksi Perusahaan       | No       |
| 72  | Identifikasi dan penanganan tantangan bisnis                 | 7               | Direksi Perusahaan       | No       |
| 73  | Penyusunan dan pemantauan anggaran perusahaan                | 7               | Direksi Perusahaan       | No       |
| 74  | Pemantauan dan pelaporan indikator kinerja keuangan          | 7               | Direksi Perusahaan       | No       |
| 75  | Pelaksanaan program pelatihan dan pengembangan karyawan      | 6               | Direksi Perusahaan       | No       |
| 76  | Mengelola hubungan dengan lembaga regulasi dan pemerintah    | 6               | Direksi Perusahaan       | No       |
| 77  | Menetapkan visi, misi, dan tujuan perusahaan                 | 6               | Direksi Perusahaan       | No       |
| 78  | Pemantauan dan evaluasi pelaksanaan tanggung jawab sosial    | 6               | Direksi Perusahaan       | No       |
| 79  | Pengembangan dan pelaksanaan kebijakan SDM                   | 6               | Direksi Perusahaan       | No       |
| 80  | Pengelolaan kepemilikan intelektual                          | 6               | Direksi Perusahaan       | No       |
| 81  | Pengawasan dan pengendalian operasional perusahaan           | 5               | Direksi Perusahaan       | No       |
| 82  | Evaluasi dan pemantauan implementasi inisiatif perusahaan    | 5               | Direksi Perusahaan       | No       |
| 83  | Pemantauan dan evaluasi kinerja vendor dan mitra             | 5               | Direksi Perusahaan       | No       |
| 84  | Pelaksanaan kebijakan privasi dan keamanan data               | 5               | Direksi Perusahaan       | No       |



# No 2

Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

*Jawaban*

<img src="photo/classDiagram.png" alt="alt text">



# No 3

Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

Jawaban

Contoh penerapan SOLID Design Principle dalam class CommentService

```java
package com.lahitani.tiktokappapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.lahitani.tiktokappapi.config.DatabaseConnection;
import com.lahitani.tiktokappapi.entity.Comment;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

@Service
public class CommentService {

    private static final String COLLECTION_NAME = "videos";

    private final Firestore dbFirestore;

    public CommentService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }

    public ResponseEntity<String> saveComment(String uid, Comment comment) {
        try {
            CollectionReference videosCollection = dbFirestore.collection(COLLECTION_NAME);
            DocumentReference videoDocRef = videosCollection.document(uid);
            CollectionReference commentsCollection = videoDocRef.collection("comments");

            String commentId = comment.getId();
            DocumentReference commentDocRef = commentsCollection.document(commentId);
            comment.setId(commentId);
            commentDocRef.set(comment);

            return ResponseEntity.ok("Comment saved successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to save comment: " + e.getMessage());
        }
    }


    public ResponseEntity<?> getCommentDetails(String uid) {
        try {
            CollectionReference videosCollection = dbFirestore.collection(COLLECTION_NAME);
            DocumentReference videoDocRef = videosCollection.document(uid);
            CollectionReference commentsCollection = videoDocRef.collection("comments");

            ApiFuture<QuerySnapshot> future = commentsCollection.get();
            QuerySnapshot querySnapshot = future.get();
            List<Comment> commentList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                Comment comment = document.toObject(Comment.class);
                commentList.add(comment);
            }

            return ResponseEntity.ok(commentList);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve comment details: " + e.getMessage());
        }
    }

    public ResponseEntity<String> updateComment(Comment comment) {
        try {
            CollectionReference videosCollection = dbFirestore.collection(COLLECTION_NAME);
            DocumentReference videoDocRef = videosCollection.document(comment.getUid());
            CollectionReference commentsCollection = videoDocRef.collection("comments");

            DocumentReference commentDocRef = commentsCollection.document(comment.getId());
            Map<String, Object> updateData = comment.toMap();
            updateData.remove("uid"); // Menghapus field UID dari data pembaruan

            // Menghapus data yang tidak berubah
            Map<String, Object> filteredData = new HashMap<>();
            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // Memastikan nilai tidak null
                if (value != null) {
                    filteredData.put(key, value);
                }
            }

            // Melakukan pembaruan hanya jika ada perubahan
            if (!filteredData.isEmpty()) {
                filteredData.put("lastUpdated", FieldValue.serverTimestamp()); // Opsional: Sertakan field timestamp

                commentDocRef.update(filteredData);

                return ResponseEntity.ok("Comment updated successfully");
            }

            return ResponseEntity.ok("No changes to update");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update comment: " + e.getMessage());
        }
    }

    public ResponseEntity<String> deleteComment(String uid, String commentId) {
        try {
            CollectionReference videosCollection = dbFirestore.collection(COLLECTION_NAME);
            DocumentReference videoDocRef = videosCollection.document(uid);
            CollectionReference commentsCollection = videoDocRef.collection("comments");

            DocumentReference commentDocRef = commentsCollection.document(commentId);
            commentDocRef.delete();

            return ResponseEntity.ok("Comment deleted successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete comment: " + e.getMessage());
        }
    }

    public ResponseEntity<?> getAllComments(String uid) {
        try {
            CollectionReference videosCollection = dbFirestore.collection(COLLECTION_NAME);
            DocumentReference videoDocRef = videosCollection.document(uid);
            CollectionReference commentsCollection = videoDocRef.collection("comments");

            ApiFuture<QuerySnapshot> future = commentsCollection.get();
            QuerySnapshot querySnapshot = future.get();
            List<Comment> commentList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                Comment comment = document.toObject(Comment.class);
                commentList.add(comment);
            }

            return ResponseEntity.ok(commentList);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve comments: " + e.getMessage());
        }
    }
}
```

Dalam class comment di atas, ada beberapa prinsip SOLID telah terlaksana sebagian, seperti:

1. Single Responsibility Principle (SRP): Kelas `CommentService` memiliki tanggung jawab yang jelas untuk mengelola komentar dalam koleksi Firestore. Ia memiliki metode yang berfokus pada tugas yang spesifik seperti menyimpan, mengambil, memperbarui, dan menghapus komentar. Prinsip SRP diikuti dengan baik.

2. Open-Closed Principle (OCP): Kelas `CommentService` tidak secara eksplisit menerapkan prinsip OCP karena tidak ada fitur yang diberikan untuk memperluas atau memodifikasi perilaku kelas ini. Namun, kelas ini dapat dengan mudah diubah atau diperluas dengan menambahkan metode baru jika diperlukan untuk memenuhi kebutuhan bisnis yang berubah.

3. Liskov Substitution Principle (LSP): Prinsip LSP berhubungan dengan substitusi objek turunan pada objek induknya. Dalam kasus ini, kelas `CommentService` tidak memiliki ketergantungan pada objek turunan dan hanya bergantung pada Firestore API. Oleh karena itu, prinsip LSP tidak relevan dalam konteks ini.

4. Interface Segregation Principle (ISP): Kelas `CommentService` tidak menerapkan prinsip ISP secara langsung, tetapi jika diperlukan, Anda dapat mempertimbangkan untuk membagi antarmuka atau mengelompokkan metode-metode yang berhubungan dengan tugas yang sama ke dalam antarmuka terpisah.

5. Dependency Inversion Principle (DIP): Kelas `CommentService` menerapkan prinsip DIP dengan baik melalui penggunaan kelas `DatabaseConnection` sebagai dependensi yang diinjeksi melalui konstruktor. Ini memungkinkan fleksibilitas dalam penggunaan sumber daya database dan memudahkan pengujian dengan penggantian dependensi.

Secara keseluruhan, meskipun tidak semua prinsip SOLID diterapkan secara langsung, namun kelas `CommentService` memenuhi beberapa prinsip dan telah dirancang dengan tanggung jawab yang terfokus.

# No 4

Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

*Jawaban*

- Singleton pattern
Kodingan dibawah ini termasuk dalam Singleton Pattern karena memenuhi karakteristik dasar dari pola desain Singleton, yaitu:
1. Private Constructor: Konstruktor pada kelas DatabaseConnection dinyatakan sebagai private. Hal ini menghindari instansiasi langsung dari luar kelas, sehingga tidak ada objek DatabaseConnection yang dapat dibuat secara langsung.
2. Static Instance: Terdapat variabel instance yang dideklarasikan sebagai private static pada kelas DatabaseConnection. Variabel ini menyimpan satu-satunya instance yang ada dari kelas DatabaseConnection.
3. getInstance() Method: Metode getInstance() digunakan untuk mendapatkan instance dari kelas DatabaseConnection. Metode ini menggunakan double-checked locking untuk memastikan hanya satu instance yang dibuat. Jika instance belum ada, maka instance baru akan dibuat; jika sudah ada, maka instance yang sudah ada akan dikembalikan.
4. Synchronized Access: Metode getInstance() diberi modifier synchronized untuk memastikan akses ke bagian pembuatan instance bersifat thread-safe. Hal ini mencegah terjadinya kondisi race condition ketika banyak thread berusaha membuat instance DatabaseConnection secara bersamaan.

```java
package com.lahitani.tiktokappapi.config;

import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.DependsOn;

/**
 * Kelas Singleton untuk mengelola koneksi ke database.
 */
@Component
@DependsOn("firebaseInitialization")
public class DatabaseConnection {
    private static DatabaseConnection instance;
    private Firestore dbFirestore;

    private DatabaseConnection() {
        // Inisialisasi koneksi ke database di sini
        dbFirestore = FirestoreClient.getFirestore();
    }

    /**
     * Mengembalikan instans dari DatabaseConnection.
     *
     * @return Instans dari DatabaseConnection
     */
    public static synchronized DatabaseConnection getInstance() {
        if (instance == null) {
            instance = new DatabaseConnection();
        }
        return instance;
    }

    /**
     * Mengembalikan instans Firestore untuk mengakses database.
     *
     * @return Instans Firestore
     */
    public Firestore getFirestore() {
        return dbFirestore;
    }
}
```

- Factory Method:
 Menggunakan factory method fromJson untuk membuat objek User dari sebuah map JSON.

```dart
factory User.fromJson(Map<String, dynamic> json) {
    return User(
      name: json['name'],
      email: json['email'],
      uid: json['uid'],
      profilePhoto: json['profilePhoto'],
    );
  }
```

# No 5

Mampu menunjukkan dan menjelaskan konektivitas ke database

*Jawaban*

- Class untuk mengelola koneksi ke database dengan singleton pattern

```java
package com.lahitani.tiktokappapi.config;

import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.DependsOn;

/**
 * Kelas Singleton untuk mengelola koneksi ke database.
 */
@Component
@DependsOn("firebaseInitialization")
public class DatabaseConnection {
    private static DatabaseConnection instance;
    private Firestore dbFirestore;

    private DatabaseConnection() {
        // Inisialisasi koneksi ke database di sini
        dbFirestore = FirestoreClient.getFirestore();
    }

    /**
     * Mengembalikan instans dari DatabaseConnection.
     *
     * @return Instans dari DatabaseConnection
     */
    public static synchronized DatabaseConnection getInstance() {
        if (instance == null) {
            instance = new DatabaseConnection();
        }
        return instance;
    }

    /**
     * Mengembalikan instans Firestore untuk mengakses database.
     *
     * @return Instans Firestore
     */
    public Firestore getFirestore() {
        return dbFirestore;
    }
}
```

- Class Videoservice mengelola database Video 

```java
@Service
public class VideoService {

    private static final String COLLECTION_NAME = "videos";
    private final Firestore dbFirestore;

    public VideoService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }
} 
```

# No 6

Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

*Jawaban*

- Saya membuat web service dengan REST dan menggunakan framework Spring boot
- Dokumentasi API dengan OpenApi: Link Dokumentasi [dokumentasi](openapi.yaml)
- Sourcode lengkap mengenai web service bisa dilihat sini -> [backend](https://gitlab.com/lahitanianissa32/webservice_pbo.git)
- Contoh code CRUD web service dan penjelasannya:

*Mengelola Video*

```java
package com.lahitani.tiktokappapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.lahitani.tiktokappapi.config.DatabaseConnection;
import com.lahitani.tiktokappapi.entity.Video;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

@Service
public class VideoService {

    private static final String COLLECTION_NAME = "videos";
    private final Firestore dbFirestore;

    public VideoService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }

    public ResponseEntity<String> saveVideo(Video video) {
        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(video.getUid())
                    .set(video);

            return ResponseEntity.ok("Video saved successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to save video: " + e.getMessage());
        }
    }

    public ResponseEntity<?> getVideoDetails(String uid) {
        try {
            DocumentReference documentReference = dbFirestore.collection(COLLECTION_NAME).document(uid);
            ApiFuture<DocumentSnapshot> future = documentReference.get();
            DocumentSnapshot documentSnapshot = future.get();

            if (documentSnapshot.exists()) {
                Video video = documentSnapshot.toObject(Video.class);
                return ResponseEntity.ok(video);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve video details: " + e.getMessage());
        }
    }

    public ResponseEntity<String> updateVideo(Video video) {
        try {
            Map<String, Object> updateData = video.toMap();
            updateData.remove("uid"); // Menghapus field UID dari data pembaruan

            // Menghapus data yang tidak berubah
            Map<String, Object> filteredData = new HashMap<>();
            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // Memastikan nilai tidak null
                if (value != null) {
                    filteredData.put(key, value);
                }
            }

            // Melakukan pembaruan hanya jika ada perubahan
            if (!filteredData.isEmpty()) {
                filteredData.put("lastUpdated", FieldValue.serverTimestamp()); // Opsional: Sertakan field timestamp

                dbFirestore.collection("videos")
                        .document(video.getUid())
                        .update(filteredData);

                return ResponseEntity.ok("Video updated successfully");
            }

            return ResponseEntity.ok("No changes to update");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update video: " + e.getMessage());
        }
    }



    public ResponseEntity<String> deleteVideo(String uid) {
        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(uid)
                    .delete();

            return ResponseEntity.ok("Video deleted successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete video: " + e.getMessage());
        }
    }

    public ResponseEntity<?> getAllVideos() {
        try {
            CollectionReference collectionReference = dbFirestore.collection(COLLECTION_NAME);
            ApiFuture<QuerySnapshot> future = collectionReference.get();
            QuerySnapshot querySnapshot = future.get();
            List<Video> videoList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                Video video = document.toObject(Video.class);
                videoList.add(video);
            }

            return ResponseEntity.ok(videoList);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve videos: " + e.getMessage());
        }
    }
}
```

 Berikut adalah penjelasan setiap operasi CRUD yang diimplementasikan dalam kode tersebut:

1. **Create (Save)**:
   - Metode: `saveVideo(Video video)`
   - Deskripsi: Metode ini digunakan untuk menyimpan (membuat) objek Video ke database Firestore.
   - Proses:
     - Menerima objek Video sebagai parameter.
     - Menggunakan Firestore untuk menyimpan objek Video ke koleksi "videos" dengan UID sebagai ID dokumen.
     - Mengembalikan respons HTTP 200 OK jika penyimpanan berhasil, atau respons HTTP 500 Internal Server Error jika terjadi kesalahan.

2. **Read (Get)**:
   - Metode: `getVideoDetails(String uid)`
   - Deskripsi: Metode ini digunakan untuk mendapatkan detail objek Video berdasarkan UID dari database Firestore.
   - Proses:
     - Menerima UID sebagai parameter.
     - Mencari dokumen dengan UID tersebut di koleksi "videos" menggunakan Firestore.
     - Mengembalikan objek Video jika dokumen ditemukan, atau respons HTTP 404 Not Found jika dokumen tidak ditemukan.
     - Mengembalikan respons HTTP 500 Internal Server Error jika terjadi kesalahan.

3. **Update**:
   - Metode: `updateVideo(Video video)`
   - Deskripsi: Metode ini digunakan untuk memperbarui objek Video di database Firestore.
   - Proses:
     - Menerima objek Video sebagai parameter.
     - Mengonversi objek Video menjadi Map menggunakan metode `toMap()`.
     - Menghapus field UID dari Map pembaruan.
     - Menghapus data yang tidak berubah dan memastikan nilai tidak null.
     - Jika ada perubahan, mengupdate dokumen dengan UID yang cocok di koleksi "videos" menggunakan Firestore.
     - Mengembalikan respons HTTP 200 OK jika pembaruan berhasil dengan atau tanpa perubahan.
     - Mengembalikan respons HTTP 500 Internal Server Error jika terjadi kesalahan.

4. **Delete**:
   - Metode: `deleteVideo(String uid)`
   - Deskripsi: Metode ini digunakan untuk menghapus objek Video dari database Firestore berdasarkan UID.
   - Proses:
     - Menerima UID sebagai parameter.
     - Menghapus dokumen dengan UID tersebut dari koleksi "videos" menggunakan Firestore.
     - Mengembalikan respons HTTP 200 OK jika penghapusan berhasil, atau respons HTTP 500 Internal Server Error jika terjadi kesalahan.

5. **Get All**:
   - Metode: `getAllVideos()`
   - Deskripsi: Metode ini digunakan untuk mendapatkan semua objek Video dari database Firestore.
   - Proses:
     - Mengambil semua dokumen dari koleksi "videos" menggunakan Firestore.
     - Mengonversi setiap dokumen menjadi objek Video dan menambahkannya ke dalam daftar videoList.
     - Mengembalikan daftar videoList jika berhasil, atau respons HTTP 500 Internal Server Error jika terjadi kesalahan.

Kode tersebut merupakan implementasi sederhana dari layanan web dengan operasi CRUD menggunakan Spring Framework dan Firestore. Dalam praktiknya, Anda dapat mengintegrasikan kode ini dengan kerangka kerja atau platform yang lebih besar untuk membangun web service yang lebih komprehensif.

```java
package com.lahitani.tiktokappapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.lahitani.tiktokappapi.entity.Video;
import com.lahitani.tiktokappapi.service.VideoService;

@RestController
@RequestMapping("/api/videos")
public class VideoController {
    private final VideoService videoService;

    @Autowired
    public VideoController(VideoService videoService) {
        this.videoService = videoService;
    }

    @PostMapping
    public ResponseEntity<String> saveVideo(@RequestBody Video video) {
        try {
            return videoService.saveVideo(video);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to save video: " + e.getMessage());
        }
    }

    @GetMapping("/{uid}")
    public ResponseEntity<?> getVideo(@PathVariable String uid) {
        try {
            return videoService.getVideoDetails(uid);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve video details: " + e.getMessage());
        }
    }

    @PutMapping("/{uid}")
    public ResponseEntity<String> updateVideo(@PathVariable String uid, @RequestBody Video video) {
        try {
            video.setUid(uid);
            return videoService.updateVideo(video);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update video: " + e.getMessage());
        }
    }

    @DeleteMapping("/{uid}")
    public ResponseEntity<String> deleteVideo(@PathVariable String uid) {
        try {
            return videoService.deleteVideo(uid);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete video: " + e.getMessage());
        }
    }
    
    @GetMapping
    public ResponseEntity<?> getAllVideos() {
        try {
            return videoService.getAllVideos();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve videos: " + e.getMessage());
        }
    }
}
```

Berikut adalah penjelasan setiap operasi CRUD yang diimplementasikan dalam controler:

1. **Create (Save)**:
   - Endpoint: `@PostMapping`
   - Deskripsi: Metode ini menerima permintaan POST untuk menyimpan (membuat) objek Video ke database.
   - Proses:
     - Menerima objek Video sebagai data permintaan dalam format JSON menggunakan anotasi `@RequestBody`.
     - Memanggil metode `saveVideo` dari kelas VideoService untuk menyimpan video.
     - Mengembalikan respons HTTP 200 OK jika penyimpanan berhasil, atau respons HTTP 500 Internal Server Error jika terjadi kesalahan.

2. **Read (Get)**:
   - Endpoint: `@GetMapping("/{uid}")`
   - Deskripsi: Metode ini menerima permintaan GET untuk mendapatkan detail objek Video berdasarkan UID.
   - Proses:
     - Menerima UID sebagai parameter dalam path variabel menggunakan anotasi `@PathVariable`.
     - Memanggil metode `getVideoDetails` dari kelas VideoService untuk mendapatkan detail video.
     - Mengembalikan objek Video jika ditemukan, atau respons HTTP 404 Not Found jika video tidak ditemukan.
     - Mengembalikan respons HTTP 500 Internal Server Error jika terjadi kesalahan.

3. **Update**:
   - Endpoint: `@PutMapping("/{uid}")`
   - Deskripsi: Metode ini menerima permintaan PUT untuk memperbarui objek Video berdasarkan UID.
   - Proses:
     - Menerima UID sebagai parameter dalam path variabel menggunakan anotasi `@PathVariable`.
     - Menerima objek Video sebagai data permintaan dalam format JSON menggunakan anotasi `@RequestBody`.
     - Mengatur UID pada objek Video sesuai dengan UID yang diterima.
     - Memanggil metode `updateVideo` dari kelas VideoService untuk memperbarui video.
     - Mengembalikan respons HTTP 200 OK jika pembaruan berhasil dengan atau tanpa perubahan, atau respons HTTP 500 Internal Server Error jika terjadi kesalahan.

4. **Delete**:
   - Endpoint: `@DeleteMapping("/{uid}")`
   - Deskripsi: Metode ini menerima permintaan DELETE untuk menghapus objek Video berdasarkan UID.
   - Proses:
     - Menerima UID sebagai parameter dalam path variabel menggunakan anotasi `@PathVariable`.
     - Memanggil metode `deleteVideo` dari kelas VideoService untuk menghapus video.
     - Mengembalikan respons HTTP 200 OK jika penghapusan berhasil, atau respons HTTP 500 Internal Server Error jika terjadi kesalahan.

5. **Get All**:
   - Endpoint: `@GetMapping`
   - Deskripsi: Metode ini menerima permintaan GET untuk mendapatkan semua objek Video dari database.
   - Proses:
     - Memanggil metode `getAllVideos` dari kelas VideoService untuk mendapatkan daftar semua video.
     - Mengembalikan daftar video jika berhasil, atau respons HTTP 500 Internal Server Error jika terjadi kesalahan.

Kode tersebut merupakan implementasi controler sederhana menggunakan Spring Framework. Kontroler ini mengatur routing dan pemrosesan permintaan HTTP untuk operasi CRUD pada objek Video. Dalam praktiknya, Anda dapat menghubungkan kontroler ini dengan bagian lain dari aplikasi web Anda untuk membangun API yang lengkap.

# No 7

Mampu menunjukkan dan menjelaskan Graphical User Interface dari
produk digital

*Jawaban*

Jawab:
Sourcode GUI dengan flutter:
[frontend](https://gitlab.com/lahitanianissa32/pbotiktok.git)

- Login

<img src="photo/Screenshot_1688999626.png" alt="alt text" width="300">

- Register

<img src="photo/Screenshot_1688999642.png" alt="alt text" width="300">

- Profile

<img src="photo/Screenshot_1688999830.png" alt="alt text" width="300">

- Upload Video

<img src="photo/Screenshot_1688999842.png" alt="alt text" width="300">

- Choose Video

<img src="photo/Screenshot_1688999845.png" alt="alt text" width="300">
<img src="photo/Screenshot_1688999849.png" alt="alt text" width="300">

- Add Song

<img src="photo/Screenshot_1688999852.png" alt="alt text" width="300">

- Add Caption

<img src="photo/Screenshot_1688999881.png" alt="alt text" width="300">

- Home Screen

<img src="photo/Screenshot_1688999941.png" alt="alt text" width="300">
<img src="photo/Screenshot_1688999963.png" alt="alt text" width="300">

- Comment Screen

<img src="photo/Screenshot_1689000013.png" alt="alt text" width="300">

- Scroll

<img src="photo/Screenshot_1689418804.png" alt="alt text" width="300">

- Share Video

<img src="photo/Screenshot_1689000030.png" alt="alt text" width="300">

- Search Users

<img src="photo/Screenshot_1689000043.png" alt="alt text" width="300">
<img src="photo/Screenshot_1689000052.png" alt="alt text" width="300">

- Follow User

<img src="photo/Screenshot_1689000060.png" alt="alt text" width="300">

- Unfollow User

<img src="photo/Screenshot_1689000066.png" alt="alt text" width="300">


# No 8

Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

*Jawaban*

- POST

```dart
void registerUser(String username, String email, String password, File? image) async {
  try {
    if (username.isNotEmpty &&
        email.isNotEmpty &&
        password.isNotEmpty &&
        image != null) {
      // save user to Firebase Authentication
      UserCredential cred = await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      // Upload the image to storage and get the download URL
      String downloadUrl = await _uploadToStorage(image);

      // Create the user object
      var user = {
        'name': username,
        'email': email,
        'uid': cred.user!.uid,
        'profilePhoto': downloadUrl,
      };

      // Convert the user object to JSON
      var userJson = json.encode(user);

      // Make a POST request to the API endpoint
      var response = await http.post(
        Uri.parse('http://10.0.2.2:8070/api/users'),
        headers: {'Content-Type': 'application/json'},
        body: userJson,
      );

      // Check the response status
      if (response.statusCode == 200) {
        // User data saved successfully
        print('User data saved successfully');
      } else {
        // Error saving user data
        print('Error saving user data: ${response.body}');
      }
    } else {
      Get.snackbar(
        'Error Creating Account',
        'Please enter all the fields',
      );
    }
  } catch (e) {
    Get.snackbar(
      'Error Creating Account',
      e.toString(),
    );
  }
} 
```

- GET

```dart
Future<void> getComment() async {
  try {
    final url = Uri.parse('http://10.0.2.2:8070/api/videos/$_postId/comments');
    final response = await http.get(url);

    if (response.statusCode == 200) {
      final List<dynamic> responseData = json.decode(response.body);
      List<Comment> comments = [];

      for (var commentData in responseData) {
        Comment comment = Comment.fromMap(commentData);
        comments.add(comment);
      }

      _comments.value = comments; // Update RxList value
    } else {
      throw Exception('Gagal memuat komentar: ${response.statusCode}');
    }
  } catch (e) {
    // Handle error
  }
}
```


# No 9

Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

[Video Demonstrasi](https://youtube.com/watch?v=HxAXnYxzGWk&feature=share7)


# No 10

BONUS !!! Mendemonstrasikan penggunaan Machine Learning
