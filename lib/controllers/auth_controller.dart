import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tiktokapp/constants.dart';
import 'package:tiktokapp/models/user.dart' as model;
import 'package:tiktokapp/views/screens/auth/login_screen.dart';
import 'package:tiktokapp/views/screens/home_screen.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class AuthController extends GetxController {
  static AuthController instance = Get.find();
  late Rx<User?> _user;
  late Rx<File?> _pickedImage;

  File? get profilePhoto => _pickedImage.value;
  User get user => _user.value!;

  @override
  void onReady() {
    super.onReady();
    _user = Rx<User?>(firebaseAuth.currentUser);
    _user.bindStream(firebaseAuth.authStateChanges());
    ever(_user, _setInitialScreen);
  }

  _setInitialScreen(User? user) {
    if (user == null) {
      Get.offAll(() => LoginScreen());
    } else {
      Get.offAll(() => const HomeScreen());
    }
  }

  void pickImage() async {
    final pickedImage =
        await ImagePicker().pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      Get.snackbar('Profile Picture',
          'You have successfully selected your profile picture!');
    }
    _pickedImage = Rx<File?>(File(pickedImage!.path));
  }

  // upload to firebase storage
  Future<String> _uploadToStorage(File image) async {
    Reference ref = firebaseStorage
        .ref()
        .child('profilePics')
        .child(firebaseAuth.currentUser!.uid);

    UploadTask uploadTask = ref.putFile(image);
    TaskSnapshot snap = await uploadTask;
    String downloadUrl = await snap.ref.getDownloadURL();
    return downloadUrl;
  }

  // registering the user

void registerUser(String username, String email, String password, File? image) async {
  try {
    if (username.isNotEmpty &&
        email.isNotEmpty &&
        password.isNotEmpty &&
        image != null) {
      // save user to Firebase Authentication
      UserCredential cred = await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      // Upload the image to storage and get the download URL
      String downloadUrl = await _uploadToStorage(image);

      // Create the user object
      var user = {
        'name': username,
        'email': email,
        'uid': cred.user!.uid,
        'profilePhoto': downloadUrl,
      };

      // Convert the user object to JSON
      var userJson = json.encode(user);

      // Make a POST request to the API endpoint
      var response = await http.post(
        Uri.parse('http://10.0.2.2:8070/api/users'),
        headers: {'Content-Type': 'application/json'},
        body: userJson,
      );

      // Check the response status
      if (response.statusCode == 200) {
        // User data saved successfully
        print('User data saved successfully');
      } else {
        // Error saving user data
        print('Error saving user data: ${response.body}');
      }
    } else {
      Get.snackbar(
        'Error Creating Account',
        'Please enter all the fields',
      );
    }
  } catch (e) {
    Get.snackbar(
      'Error Creating Account',
      e.toString(),
    );
  }
}


  void loginUser(String email, String password) async {
    try {
      if (email.isNotEmpty && password.isNotEmpty) {
        await firebaseAuth.signInWithEmailAndPassword(
            email: email, password: password);
      } else {
        Get.snackbar(
          'Error Logging in',
          'Please enter all the fields',
        );
      }
    } catch (e) {
      Get.snackbar(
        'Error Loggin gin',
        e.toString(),
      );
    }
  }

  void signOut() async {
    await firebaseAuth.signOut();
  }
}
